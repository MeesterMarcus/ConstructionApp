import { ConstructionAppPage } from './app.po';

describe('construction-app App', function() {
  let page: ConstructionAppPage;

  beforeEach(() => {
    page = new ConstructionAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
